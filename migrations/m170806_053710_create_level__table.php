<?php

use yii\db\Migration;

/**
 * Handles the creation of table `level_`.
 */
class m170806_053710_create_level__table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('level_', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('level_');
    }
}
