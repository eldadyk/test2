<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_`.
 */
class m170806_053731_create_status__table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status_', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status_');
    }
}
