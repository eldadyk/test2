<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170806_054804_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
