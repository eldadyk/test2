<?php

use yii\db\Migration;

/**
 * Handles the creation of table `level`.
 */
class m170806_053842_create_level_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('level', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('level');
    }
}
