<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\status;


$this->title = 'Breakdowns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breakdown-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Breakdown', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
				'attribute' => 'level',
				'label' => 'Level',
				'value' => function($model){
							return $model->levelItem->name;
					},	
				
			],
			
          [
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
							return $model->statusItem->name;
					},	
				'filter'=>Html::dropDownList('BreakdownSearch[status]', 
				$status, $statuses, ['class'=>'form-control']),					
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
